#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from pybtex.database.input import bibtex
from habanero import Crossref
import requests.exceptions
import json
import argparse
import logging as log


def lookup_crossref(title, author):
    """Looks up the bibentry on crossref for an item

    Parameters
    ----------
    title : str
        The title of the item to look up
    author : pybtex.database.Person
        The author of the item to look up

    Returns
    -------
    TYPE
        The bibtex item if found on crossref
    """

    cr = Crossref()
    try:
        w = cr.works(query="{} {}".format(title, author))
        return w['message']['items']
    except requests.exceptions.HTTPError as e:
        log.warning(e)
        return None


def lookup_json(title, author):
    """Looks up the bibentry in data.json for testing purposes.

    Parameters
    ----------
    title : str
        The title of the item to look up
    author : pybtex.database.Person
        The author of the item to look up

    Returns
    -------
    TYPE
        The bibtex item if found in data.json
    """
    print("{}, {}".format(type(title), type(author)))
    with open("data.json") as file:
        return json.load(file)


def process_bibentry(bibkey, bibentry):
    """Tries to find the DOI and puts it into the bibentry.

    Parameters
    ----------
    bibkey : str
        The bibkey
    bibentry : pybtex.database.Entry
        The bibentry

    Returns
    -------
    Nothing
        Nothing
    """

    if 'doi' not in bibentry.fields:
        log.info("Looking up DOI for {}".format(bibkey))

        result = lookup(bibentry.fields['title'],
                        bibentry.persons['author'][0])

        if result is None:
            log.info("Could not find DOI for {}".format(bibkey))
            return

        for item in result:
            if "title" in item:
                if "DOI" in item:
                    if bibentry.fields['title'].lower() == item['title'][0].lower():
                        # update entry
                        bibentry.fields['doi'] = item['DOI']
                        log.debug("Updated DOI for {} to {}".format(
                            bibkey, item['DOI']))
                        break
                else:
                    log.debug("No DOI in {}".format(item))
            else:
                log.debug("No title in {}".format(item))


def process_bibfile(bibfile, overwrite):
    """Process an entire bibfile, update entries with DOI and save the
                file.

    Parameters
    ----------
    bibfile : file
        The bibfile
    overwrite : boolean
        Whether to overwrite the original file
    """

    # load bibfile
    parser = bibtex.Parser()
    bibdata = parser.parse_file(bibfile)

    for bibkey, bibentry in bibdata.entries.items():
        process_bibentry(bibkey, bibentry)

    outfile_name = bibfile.name
    if not overwrite:
        outfile_name = "output_{}".format(bibfile.name)
    log.info("Writing output to {}".format(outfile_name))
    bibdata.to_file(outfile_name, bib_format="bibtex")


if __name__ == '__main__':

    log.basicConfig(format="%(levelname) -8s %(message)s", level=log.DEBUG)
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=open, help="The file to process")
    parser.add_argument("--overwrite", "-o",
                        action="store_true", help="Overwrite input file")
    parser.add_argument("-t", action="store_true",
                        help="Testmode. Load entries from data.json")
    args = parser.parse_args()

    if args.t:
        lookup = lookup_json
        log.debug("Using lookup_json")
    else:
        lookup = lookup_crossref
        log.debug("Using lookup_crossref")

    process_bibfile(args.file, args.overwrite)
