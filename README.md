# Reverse DOI Lookup

Takes a bibtex file and iterates through the entries. For each entry without a DOI, the entry is looked up via Crossref and the DOI added to the local library if available.